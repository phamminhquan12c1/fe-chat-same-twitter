
const routes = [
    {
        path: '/',
        name: 'home',
        meta: {
            layout: 'home'
        },
        // route level code-splitting
        // this generates a separate chunk (About.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: ()=>import("../views/Home-view.vue")
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            layout: 'auth'
        },
        // route level code-splitting
        // this generates a separate chunk (About.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: ()=>import("../views/Login-view.vue")
    }
]
export default routes
