import { Router, createRouter, createWebHistory } from 'vue-router'
import routes from './routes'
import Fetch from '@/utils/fetch'
import { GET_ME } from '@/constants/api'
import { get } from 'lodash'

const router: Router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
})

router.beforeEach(async (to, from, next) => {
    // Nếu là trang login
    if (to.name === 'login') {
        if (localStorage.getItem('access_token')) {
            return next({ name: 'home' })
        }
        const access_token = to.query.access_token
        const refresh_token = to.query.refresh_token

        if (access_token && refresh_token) {
            localStorage.setItem('access_token', access_token as string)
            localStorage.setItem('refresh_token', refresh_token as string)
            const user = await Fetch.get(GET_ME, { authHeader: true })
            if (get(user, 'statusCode') == 200) {
                return next({ name: 'home' })
            } else {
                localStorage.clear()
            }
        }
    }

    // Nếu không có token và truy cập vào trang không phải login, chuyển hướng về trang login
    if (to.name !== 'login' && !localStorage.getItem('access_token')) {
        return next({ name: 'login' })
    } else {
        return  next()
    }
})
export default router
