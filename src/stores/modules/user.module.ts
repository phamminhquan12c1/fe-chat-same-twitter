interface User {
    name: string;
    email: string;
}

interface State {
    user: User | null;
}

const state = (): State => ({
    user: null
});

const mutations = {
    SetUser: (state: State, user: User) => {
        state.user = user;
    }
};

const actions = {
    SetUser({ commit }: { commit: Function }, user: User) {
        commit('SetUser', user);
    }
};

export default {
    state,
    mutations,
    actions
};
