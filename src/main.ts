import { createApp, App as VueApp, defineComponent } from 'vue'
import App from './App.vue'
import router from './routes'
import { registerComponent } from './utils/imports'
import './style.css'
import './assets/css/index.css'
import store from '@/stores'
const app: VueApp<Element> = createApp(App as ReturnType<typeof defineComponent>)

registerComponent(app)

app.use(store)
app.use(router)
app.mount('#app')
