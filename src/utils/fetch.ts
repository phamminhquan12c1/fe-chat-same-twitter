import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'
import qs from 'qs'
import Router from '../routes'

class Fetch {
    options: any
    constructor() {
        this.options = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        }
    }
    // Get method
    async get(
        url: string,
        arg: { loop?: number; query?: any; authHeader?: boolean } = {
            loop: 3,
            query: {},
            authHeader: false
        }
    ) {
        delete this.options.body

        this.options.method = 'GET'

        let { loop, authHeader, query } = arg

        loop = parseInt(loop as any, 10)
        if (isNaN(loop)) {
            loop = 3
        }

        if (authHeader) {
            this.options.headers['Authorization'] = this.authHeader()
        }

        let _qs = qs.stringify(query)
        if (!_isEmpty(_qs)) {
            url += `?${_qs}`
        }

        for (let i = 1; i <= loop; i++) {
            try {
                let response = await this.fetch(url, this.options)
                let result = await this.parseJsonResponse(response)

                return result
            } catch (err) {
                if (i === loop) {
                    throw new Error(err as string)
                }
            }
        }
    }

    // Post method
    async post(
        url: string,
        body: any,
        arg: { authHeader?: boolean } = {
            authHeader: false
        }
    ) {
        const typeArg = typeof arg
        if (typeArg != 'object') {
            throw new Error('argument must be object')
        }

        this.options.body = JSON.stringify(body)

        if (_get(arg, 'authHeader') == true) {
            this.options.headers['Authorization'] = this.authHeader()
        }

        try {
            let response = await this.fetch(url, this.options)
            let result = await this.parseJsonResponse(response)
            return result
        } catch (err) {
            throw new Error(err as string)
        }
    }

    // Put method
    async put(url: string, body: any, arg: { authHeader?: boolean } = { authHeader: false }) {
        this.options.method = 'PUT'

        const typeArg = typeof arg
        if (typeArg != 'object') {
            throw new Error('argument must be object')
        }

        this.options.body = JSON.stringify(body)

        if (_get(arg, 'authHeader') == true) {
            this.options.headers['Authorization'] = this.authHeader()
        }

        try {
            let response = await this.fetch(url, this.options)
            let result = await this.parseJsonResponse(response)

            return result
        } catch (err) {
            throw new Error(err as string)
        }
    }

    // Delete method
    async delete(url: string, arg: { authHeader?: boolean } = { authHeader: false }) {
        delete this.options.body
        this.options.method = 'DELETE'

        const typeArg = typeof arg
        if (typeArg != 'object') {
            throw new Error('argument must be object')
        }

        if (_get(arg, 'authHeader') == true) {
            this.options.headers['Authorization'] = this.authHeader()
        }

        try {
            let response = await this.fetch(url, this.options)
            let result = await this.parseJsonResponse(response)

            return result
        } catch (err) {
            throw new Error(err as string)
        }
    }

    // Fetch data
    async fetch(url: string, options: any) {
        let response = await fetch(url, options)

        return response
    }

    // Parse JSON response
    async parseJsonResponse(response: Response) {
        if (!response.ok || response.status == 401) {
            return
        }

        let text = await response.text()
        const data = text && JSON.parse(text)

        if (400 <= data.code && data.code < 500) {
            let currentRouteName = _get(Router, 'history.current.name')
            if ('login' != currentRouteName) {
                localStorage.clear()
                sessionStorage.clear()

                Router.replace({ name: 'login' })
            }

            return
        }

        return data
    }

    // Parse Blob
    async parseBlob(response: Response) {
        if (!response.ok || response.status == 401) {
            let currentRouteName = _get(Router, 'history.current.name')
            if (currentRouteName != 'home') {
                Router.replace({ name: 'home' })
            }

            return
        }

        let blob = await response.blob()
        return blob
    }

    authHeader() {
        console.log('Bearer ' + localStorage.getItem('access_token'))
        return 'Bearer ' + localStorage.getItem('access_token')
    }
}

const FetchExport = new Fetch()

export default FetchExport
