import { defineAsyncComponent, App  } from "vue";

export function registerComponent(app: App<Element>) {
    app.component('home-layout', defineAsyncComponent(()=>import("../layouts/home-layout.vue")))
    app.component('auth-layout', defineAsyncComponent(()=>import("../layouts/auth-layout.vue")))
}