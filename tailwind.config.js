/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    extend: {
      colors: {
        'primary': '#00a8ff',
      }
    },
  },
  purge: ['./index.html', './src/**/*.{js,jsx,ts,tsx,vue}'],
  plugins: [],
}

